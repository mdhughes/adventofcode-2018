# adventofcode-2018

[Advent of Code 2018 blog post](https://mdhughes.tech/2018/11/29/advent-of-code-2018/)

You will need [Chicken Scheme 5](https://call-cc.org), and install each of the eggs in requirements.txt:

% chicken-install -s EGGNAME
