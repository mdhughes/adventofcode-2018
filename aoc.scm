#!/usr/bin/env csi -ss
;; aoc.scm
;; Created 2018-12-01
;; Copyright © 2018 by Mark Damon Hughes. All Rights Reserved.

(import scheme)
(import (chicken base))
(import (chicken condition))
(import (chicken fixnum))
(import (chicken format))
(import (chicken io))
(import (chicken irregex))
(import (chicken pathname))
(import (chicken process-context))
(import (chicken read-syntax))
(import (chicken sort))
(import (chicken string))
(import (chicken time))
;; eggs
(import miscmacros)
(import srfi-1) ;; lists
(import srfi-13) ;; strings
(import srfi-69) ;; NICE. hash-tables
(import srfi-133) ;; vectors
(import test)

(cond-expand
	(compiling (declare (uses marklib)))
	(else (load (make-pathname (nth-value 0 (decompose-pathname (program-name))) "marklib.scm")))
)
(import marklib)

;;______________________________________
;; Day 01

(define (parse-change change)
	{let* [(change-sign (anyitem change 0))  (change-num (atoi (substring change 1)))]
		(cond
			[(char=? #\+ change-sign)  change-num]
			[(char=? #\- change-sign)  (- change-num)]
			[else (error 'parse-change "Invalid change " change)]
		)
	}
)

;; Standard loop over list. Only slightly complicated by atoi not handling + in number strings.
(define (aoc01 datatext)
	{let loop [(freq 0)  (data (string-split datatext))]
		(cond
			[(null? data)  freq]
			[else (loop (+ freq (parse-change (car data)))  (cdr data))]
		)
	}
)

;; Same thing. Loops through data by reparsing the text. Mark smart like caveman.
(define (aoc01b datatext)
	{let loop [(freq 0)  (hist (hash-number))  (data '())]
		(when (null? data)  (set! data (string-split datatext))) ;; duplicate list if not found yet
		(hadd! hist freq 1)
		(cond
			[(>= (hget hist freq 0) 2)  freq]
			[else (loop (+ freq (parse-change (car data)))  hist  (cdr data))]
		)
	}
)

;;______________________________________
;; Day 02

(define (letter-diffs a b)
	{let [(count 0)]
		{do [(i 0 (add1 i))] [(>= i (min (anylen a) (anylen b)))]
			(when (not (char=? (anyitem a i) (anyitem b i)))  (inc! count))
		}
		count
	}
)

(define (letter-hist s)
	{let [(hist (hash-string))]
		(string-for-each (lambda (c) (hadd! hist (string c) 1))  s)
		hist
	}
)

;; Didn't have hash table tools to test this puzzle, so had to write those first.
(define (aoc02 datatext)
	{let loop [(two 0)  (three 0)  (data (string-split datatext))]
		(cond
			[(null? data)  (* two three)]
			[else {let* [(hist (letter-hist (car data)))  (hist-values (hash-table-values hist))]
				(when (any (lambda (v) (= v 2)) hist-values)  (inc! two))
				(when (any (lambda (v) (= v 3)) hist-values)  (inc! three))
				(loop two three (cdr data))
			}]
		)
	}
)

;; Totally unrelated problem to first part. I'm doing an O(n^2) search, which you should never do, but still took minimal time.
(define (aoc02b datatext)
	{let [(data (list->vector (string-split datatext)))  (match1 #f)  (match2 #f)]
		{do [(i 0 (add1 i))] [(>= i (anylen data))]
			{do [(j 0 (add1 j))] [(>= j (anylen data))]
				(when (and (!= i j)  (= (letter-diffs (anyitem data i) (anyitem data j)) 1))
						(set! match1 (anyitem data i))  (set! match2 (anyitem data j))
				)
			}
		}
		(list match1 match2)
	}
)

;;______________________________________
;; Day 03

(define cloth-size 1001) ;; ignoring 0-index row/column
(define cloth-color-conflict -1)

(define (parse-rect line)
	{let* [(match (irregex-match "^#\\d+ @ (\\d+),(\\d+): (\\d+)x(\\d+)$" line))
			(x (atoi (irregex-match-substring match 1)))   (y (atoi (irregex-match-substring match 2)))
			(w (atoi (irregex-match-substring match 3)))   (h (atoi (irregex-match-substring match 4)))]
		(cond
			[(and (irregex-match-data? match) (= 4 (irregex-match-num-submatches match)))  (make-Rect x y w h)]
			[else (error 'parse-rect "Bad rect data:" line)]
		)
	}
)

(define (cloth-count cloth testable)
	{let [(count 0)]
		(Rect-for-each (Tilemap-bounds cloth) (lambda (pt)
			(when (testable (Tilemap-gridat cloth pt))  (inc! count))
		))
		count
	}
)

;; Paints a tilemap with "color" (1-indexed number of the rect) or cloth-color-conflict if already painted.
;; Then scan the tilemap again for conflict cells.
(define (aoc03 datatext)
	{let [(data (map parse-rect (string-split datatext "\r\n")))  (cloth (Tilemap-init cloth-size cloth-size 0))  (color 0)]
		(for-each (lambda (rect)
				(inc! color)
				(Rect-for-each rect (lambda (pt)
					(Tilemap-gridat-set! cloth pt (if (zero? (Tilemap-gridat cloth pt)) color cloth-color-conflict))
				))
			)
			data
		)
		(cloth-count cloth (lambda (x) (= x cloth-color-conflict)))
	}
)

;; originally I tried using the color-painting algorithm, but I had problems so brute-forced intersection tests again.
;; What could be done is if any conflict happens while painting, repaint the entire rect in conflict color.
;; Then search for any non-conflict color.
(define (aoc03b datatext)
	{let [(data (list->vector (map parse-rect (string-split datatext "\r\n"))))  (no-intersects '())]
		{do [(i 0 (add1 i))] [(>= i (anylen data))]
			{let [(intersects 0)]
				{do [(j 0 (add1 j))] [(>= j (anylen data))]
					{let [(arect (vector-ref data i))  (brect (vector-ref data j))]
						(when (and (!= i j) (Rect-intersect? arect brect))  (inc! intersects))
					}
				}
				(when (zero? intersects)  (push! (list (add1 i) (vector-ref data i)) no-intersects))
			}
		}
		no-intersects
	}
)

;;______________________________________
;; Day 04

(define (timetable-entry timetable guard sleep-time end-time)
	{let [(row (hget timetable guard))]
		(when (not row)  (set! row (make-vector 60 0))  (hput! timetable guard row))
		{do [(mm sleep-time (add1 mm))] [(>= mm end-time)]
			(vector-set! row mm (add1 (vector-ref row mm)))
		}
	}
)

;; Not especially hard to parse, though I hate writing regex without regex-strings like JS, Python, etc.
(define (parse-guard-timetable datatext)
	{let [(data (sort (string-split datatext "\r\n") anyless?))  (guard -1)  (sleep-time -1)  (timetable (hash-number))]
		(for-each (lambda (line)
			{let* [(match (irregex-match "^\\[(\\d+)-(\\d+)-(\\d+) (\\d+):(\\d+)\\] (.*)$" line))
					(hh (atoi (irregex-match-substring match 4)))  (mm (atoi (irregex-match-substring match 5)))
					(text (irregex-match-substring match 6))
				]
				(cond
					[(substring=? text "Guard #" 0)  (set! guard (atoi text))  (set! sleep-time -1)]
					[(string=? text "falls asleep")  (set! sleep-time mm)]
					[(string=? text "wakes up")  (timetable-entry timetable guard sleep-time mm)]
					[else (error 'aoc04 "Invalid guard log " line)]
				)
			})
			data
		)
		timetable
	}
)

;; Just to visualize, I wrote all this nice text UI. Then the answer was just on the table.
;; Part 2 wasn't even needed, since the answer was right there.
;; Lesson: Always spend the time to make nice ASCII art, so you don't have to think harder.
(define (aoc04 datatext debug)
	(when debug
		;; nice header
		(display "\t")
		{do [(mm 0 (add1 mm))] [(>= mm 60)]
			(format #t " ~A " (quotient mm 10))
		}
		(newline)
		(display "\t")
		{do [(mm 0 (add1 mm))] [(>= mm 60)]
			(format #t " ~A " (modulo mm 10))
		}
		(newline)
		(display "\t")
		{do [(mm 0 (add1 mm))] [(>= mm 60)]
			(display "-- ")
		}
		(newline)
	)

	{let [(timetable (parse-guard-timetable datatext))  (highest-guard -1)  (highest-guard-minutes 0)  (guard-sleepy-minute (hash-number))]
		(hash-table-for-each timetable (lambda (guard row)
			(when debug (format #t "#~A:\t" guard))
			{let [(sleepiest-minute -1)  (sleepiest-minute-count 0)]
				{do [(mm 0 (add1 mm))] [(>= mm 60)]
					(when (> (vector-ref row mm) sleepiest-minute-count)
						(set! sleepiest-minute mm)  (set! sleepiest-minute-count (vector-ref row mm))
					)
					(when debug (format #t "~A " (padnum (vector-ref row mm) 2)))
				}
				(when debug (format #t "\tSleepiest at: ~A (~Ax)~%" sleepiest-minute sleepiest-minute-count))
				(hput! guard-sleepy-minute guard sleepiest-minute)
			}

			{let [(minutes (sum (vector->list row)))]
				(when (> minutes highest-guard-minutes)
					(set! highest-guard guard)  (set! highest-guard-minutes minutes)
				)
			}
		))
		;; summary
		(when debug (format #t "Sleepiest Guard: ~A~%" highest-guard))

		(list highest-guard (hget guard-sleepy-minute highest-guard))
	}
)

;;______________________________________
;; Day 05

;; Data set is ridiculously large and impossible to debug with.
;; encode polymer as a vector of chars, - for removed bases
;; Anoyance: Scheme vectors are fixed-length, and it's maximally efficient but infuriating to work with.
;; But lists aren't appropriate, other types would be difficult; I could write a sparse array with a hash-number and renumbering every index
;; when one is "removed", like JavaScript does. Bleh.
;; Initial thought: Do everything with regex! Repeatedly regexing a 50k string was a bad idea.
(define empty-polymer #\-)

(define (polydata->string polydata)
	(irregex-replace/all "-" (vector->string polydata) "")
)

;; scan forward through bases until non-empty cell is found, -1 if EOF
(define (next-polymer-base polydata start end)
	{let loop [(i start)]
		(cond
			[(or (< i 0) (>= i end))  -1]
			[(not (char=? empty-polymer (vector-ref polydata i)))  i]
			[(< i (sub1 end))  (loop (add1 i))]
			[else  -1]
		)
	}
)

;; Ugly GOTO-like `loop`; loops through until EOF, then tries again at start until no matches happened.
;; Data set is so huge, nice output is a luxury I can't always afford, so passing `debug` turns that on.
(define (polymer-reduce polymer debug)
	{let [(polydata (string->vector polymer))]
		{let loop [(i 0)  (polylen (vector-length polydata))  (match #f)  (match-count 0)]
			{let* [(j (next-polymer-base polydata (add1 i) polylen))  (c1 (if (>= i 0) (vector-ref polydata i) empty-polymer))  (c2 (if (>= j 0) (vector-ref polydata j) empty-polymer))]
				(set! match (and (char-alphabetic? c1)  (char-alphabetic? c2)  (= 32 (abs (- (char->integer c1) (char->integer c2)))) ))
				(when match  (vector-set! polydata i empty-polymer)  (vector-set! polydata j empty-polymer)  (inc! match-count)
					(when debug (format #t "~A:~A ~A ~A:~A~%" i c1 (if match "==" "!=") j c2))
				)
			}
			(cond
				[(and (>= i 0)  (< i (sub1 polylen)))  (loop (next-polymer-base polydata (add1 i) polylen) polylen match match-count)]
				[(> match-count 0)
					(when debug {let [(s (polydata->string polydata))]
						(format #t "this pass: (~A matches, ~A bases)~%" match-count (string-length s))
					})
					(loop 0 polylen #f 0)]
				[else (polydata->string polydata)]
			)
		}
	}
)

(define (aoc05 datatext debug)
	{let [(new-polymer (polymer-reduce (string-trim-both datatext) debug))]
		(string-length new-polymer)
	}
)

(define (aoc05b datatext debug)
	{let [(results (hash-string))]
		{do [(c 0 (add1 c))] [(>= c 26)]
			{let* [(bases (format #f "[~A~A]" (integer->char (+ 65 c)) (integer->char (+ 97 c))))
					(new-polymer (polymer-reduce (string-trim-both (irregex-replace/all bases datatext "")) debug))]
				(hput! results bases (string-length new-polymer))
			}
		}
		results
	}
)

;;______________________________________
;; Day 06
;; Straightforward map-making, but a lot of nested tests & loops.
;; Pushed me to make a halfway-decent Tilemap-string.

(define (parse-point line)
	{let* [(match (irregex-match "^(\\d+), *(\\d+)$" line))
			(x (atoi (irregex-match-substring match 1)))   (y (atoi (irregex-match-substring match 2)))
		]
		(cond
			[(and (irregex-match-data? match) (= 2 (irregex-match-num-submatches match)))  (make-Point x y)]
			[else (error 'parse-point "Bad point data:" line)]
		)
	}
)

(define (coord-distance-shortest apt data)
	{let [(index -1)  (shortest-indices '())  (shortest-distance most-positive-fixnum)]
		(for-each (lambda (bpt)
				(inc! index)
				{let [(distance (+ (abs (- (Point-x apt) (Point-x bpt)))  (abs (- (Point-y apt) (Point-y bpt))) ))]
					(cond
						[(eq? apt bpt)  #f]
						[(< distance shortest-distance)  (set! shortest-indices (list index))  (set! shortest-distance distance) ]
						[(= distance shortest-distance)  (push! index shortest-indices) ]
					)
				}
			)
			data
		)
		(cond
			[(!= (length shortest-indices) 1)  '(-1 -1)]
			[else (list (car shortest-indices)  shortest-distance)]
		)
	}
)

(define (coord-distance-total apt data)
	(sum (map (lambda (bpt) (+ (abs (- (Point-x apt) (Point-x bpt)))  (abs (- (Point-y apt) (Point-y bpt)))) ) data))
)

(define (aoc06 datatext debug)
	{let* [(data (map parse-point (string-split datatext "\r\n")))
			(width (+ 2 (apply max (map (lambda (pt) (Point-x pt)) data))))
			(height (+ 2 (apply max (map (lambda (pt) (Point-y pt)) data))))
			(coords (if debug (Tilemap-init width height '(#f 0)) #f))
			(pt (make-Point 0 0))  (area (hash-number))
		]
		(when debug (format #t "Coords size: ~A x ~A~%" width height))
		;; coords tilemap of (index distance) isn't needed for this, but nice to see in debug.
		;; area counts grids of indices not touching the edge
		{do [(y 0 (add1 y))] [(>= y height)]
			(Point-y-set! pt y)
			{do [(x 0 (add1 x))] [(>= x width)]
				(Point-x-set! pt x)
				{let* [(dist (coord-distance-shortest pt data))  (index (car dist))]
					(when debug (Tilemap-gridat-set! coords pt dist))
					(cond
						[(= index -1) #f] ;; empty
						[(= (hget area index 0) -1) #f] ;; invalid
						[(or (<= x 0)  (>= x (sub1 width))  (<= y 0)  (>= y (sub1 height)))
							(hput! area index -1)] ;; edge, mark invalid
						[else (hadd! area index 1)]
					)
				}
			}
		}
		(when debug (display (Tilemap-string coords (lambda (g) (pad (->string g) -7))) ))
		area
	}
)

(define (aoc06b datatext safe-distance debug)
	(when debug (newline))
	{let* [(data (map parse-point (string-split datatext "\r\n")))
			(width (+ 2 (apply max (map (lambda (pt) (Point-x pt)) data))))
			(height (+ 2 (apply max (map (lambda (pt) (Point-y pt)) data))))
			(coords (Tilemap-init width height 0))
			(pt (make-Point 0 0))  (area 0)
		]
		(when debug (format #t "Coords size: ~A x ~A~%" width height))
		;; coords tilemap of distance
		;; area counts grids under safe-distance
		{do [(y 0 (add1 y))] [(>= y height)]
			(Point-y-set! pt y)
			{do [(x 0 (add1 x))] [(>= x width)]
				(Point-x-set! pt x)
				{let [(dist (coord-distance-total pt data))]
					(when (< dist safe-distance)  (Tilemap-gridat-set! coords pt dist)  (inc! area))
				}
			}
		}
		(when debug (display (Tilemap-string coords (lambda (g) (pad (->string g) 4)))))
		area
	}
)

;;______________________________________
;; Day 07
;; I hate making btrees, which is presumably the "correct" solution, so I just treated it as a mini-language.
;; But while my test passed, my first result was rejected: QCFJKONPBSEUMWXADLYIGVRHT
;; Ah, I wasn't sorting the tasks alphabetically and working the roots every time.
;; … Total rewrite is now massively simpler, clearly overthinking it.
;; Part 2 is evil, fucking threading?  Gonna punt on this one for now.

(define (parse-ikea-sleigh line)
	{let* [(match (irregex-match "^Step (\\w+) must be finished before step (\\w+) can begin.$" line))
			(req (irregex-match-substring match 1))   (step (irregex-match-substring match 2))
		]
		(cond
			[(and (irregex-match-data? match) (= 2 (irregex-match-num-submatches match)))  (list req step)]
			[else (error 'parse-ikea-sleigh "Bad sleigh instruction data:" line)]
		)
	}
)

(define (aoc07 datatext debug)
	{let [(data (map parse-ikea-sleigh (string-split datatext "\r\n")))
			(requires (hash-string))  (todolist '())  (donelist '())]
		(for-each (lambda (row)
				{let* [(req (car row))  (step (cadr row))  (todolist (hget requires step))]
					(hput! requires step (if todolist (append todolist (list req))  (list req)))
					(when (not (hget requires req))  (hput! requires req '()))  ;; make sure requirements are seen, but don't count
				}
			)
			data
		)
		(set! todolist (sort (hash-table-keys requires) anyless?))
		{while (notnull? todolist)
			; pick first item with no requirements
			{let [(step (find (lambda (key) (null? (hget requires key)) ) todolist))]
				(when (not step) (error 'aoc07 "Expected step from todolist: " todolist))
				; move to donelist
				(list-append! donelist step)
				(list-delete! todolist step)
				; remove item from all other requirement lists
				(hash-table-for-each requires (lambda (key value)
					(hput! requires key (delete step value))
				))
			}
		}
		(string-intersperse donelist "")
	}
)

;; Not a solution, just a placeholder for computing cost
(define (aoc07b datatext seconds-base debug)
	{let [(s (aoc07 datatext debug))  (t 0)]
		(for-each (lambda (c)
				(inc! t (+ seconds-base (- (char->integer c) 64)))
			)
			(string->list s)
		)
		t
	}
)

;;______________________________________

(define (all-tests)
	(format #t "Usage: aoc.scm DAY INPUTFILE~%")
	(test-group "aoc01"
		(test "aoc01" 3 (aoc01 "+1\n-2\n+3\n+1\n"))
		(test "aoc01b" 2 (aoc01b "+1\n-2\n+3\n+1\n"))
		(test "aoc01b" 0 (aoc01b "+1\n-1\n"))
		(test "aoc01b" 10 (aoc01b "+3\n+3\n+4\n-2\n-4\n"))
		(test "aoc01b" 5 (aoc01b "-6\n+3\n+8\n+5\n-6\n"))
		(test "aoc01b" 14 (aoc01b "+7\n+7\n-2\n-7\n-4\n"))
	)
	(test-group "aoc02"
		(test "aoc02" "a: 1\nb: 1\nc: 1\nd: 1\ne: 1\nf: 1\n" (hstring (letter-hist "abcdef")))
		(test "aoc02" "a: 2\nb: 3\nc: 1\n" (hstring (letter-hist "bababc")))
		(test "aoc02" "a: 1\nb: 2\nc: 1\nd: 1\ne: 1\n" (hstring (letter-hist "abbcde")))
		(test "aoc02" "a: 1\nb: 1\nc: 3\nd: 1\n" (hstring (letter-hist "abcccd")))
		(test "aoc02" "a: 2\nb: 1\nc: 1\nd: 2\n" (hstring (letter-hist "aabcdd")))
		(test "aoc02" "a: 1\nb: 1\nc: 1\nd: 1\ne: 2\n" (hstring (letter-hist "abcdee")))
		(test "aoc02" "a: 3\nb: 3\n" (hstring (letter-hist "ababab")))
		(test "aoc02" 12 (aoc02 "abcdef\nbababc\nabbcde\nabcccd\naabcdd\nabcdee\nababab\n"))
		(test "aoc02" 2 (letter-diffs "abcde" "axcye"))
		(test "aoc02" 1 (letter-diffs "fghij" "fguij"))
		(test "aoc02b" '("fguij" "fghij") (aoc02b "abcde\nfghij\nklmno\npqrst\nfguij\naxcye\nwvxyz\n"))
	)
	(test-group "aoc03"
		{let [(arect (make-Rect 1 3 4 4)) (brect (make-Rect 3 1 4 4)) (crect (make-Rect 5 5 2 2))]
			(test "Rect-intersect" (make-Rect 3 3 2 2) (Rect-intersect arect brect))
			(test "Rect-intersect" (make-Rect 0 0 0 0) (Rect-intersect arect crect))
			(test "Rect-intersect" (make-Rect 0 0 0 0) (Rect-intersect brect crect))
			;slow, don't bother testing again (test "aoc03" 4 (aoc03 "#1 @ 1,3: 4x4\n#2 @ 3,1: 4x4\n#3 @ 5,5: 2x2\n"))
			(test "aoc03b" (list (list 3 (make-Rect 5 5 2 2))) (aoc03b "#1 @ 1,3: 4x4\n#2 @ 3,1: 4x4\n#3 @ 5,5: 2x2\n"))
		}
	)
	(test-group "aoc04"
		(test "aoc04" #<<END
10: #(0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0)
99: #(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 2 2 2 2 2 3 2 2 2 2 1 1 1 1 1 0 0 0 0 0)
END
			(string-trim-right (hstring (parse-guard-timetable (read-text-file "aoc-2018/input-test04.txt"))))
		)
		(test "aoc04" '(10 24) (aoc04 (read-text-file "aoc-2018/input-test04.txt") #f))
	)
	(test-group "aoc05"
		(test "aoc05" "dabCBAcaDA" (polymer-reduce "dabAcCaCBAcCcaDA" #f))
		(test "aoc05" 10 (aoc05 "dabAcCaCBAcCcaDA" #f))
		(test "aoc05b" "[Aa]: 6\n[Bb]: 8\n[Cc]: 4\n[Dd]: 6\n[Ee]: 10\n[Ff]: 10\n[Gg]: 10\n[Hh]: 10\n[Ii]: 10\n[Jj]: 10\n[Kk]: 10\n[Ll]: 10\n[Mm]: 10\n[Nn]: 10\n[Oo]: 10\n[Pp]: 10\n[Qq]: 10\n[Rr]: 10\n[Ss]: 10\n[Tt]: 10\n[Uu]: 10\n[Vv]: 10\n[Ww]: 10\n[Xx]: 10\n[Yy]: 10\n[Zz]: 10\n" (hstring (aoc05b "dabAcCaCBAcCcaDA" #f)))
	)
	(test-group "aoc06"
		(test "aoc06" "0: -1\n1: -1\n2: -1\n3: 9\n4: 17\n5: -1\n" (hstring (aoc06 "1, 1\n1, 6\n8, 3\n3, 4\n5, 5\n8, 9\n" #f)))
		(test "aoc06" 16 (aoc06b "1, 1\n1, 6\n8, 3\n3, 4\n5, 5\n8, 9\n" 32 #f))
	)
	(test-group "aoc07"
		(test "aoc07" "CABDFE" (aoc07 (read-text-file "aoc-2018/input-test07.txt") #t))
		(test "aoc07b" 15 (aoc07b (read-text-file "aoc-2018/input-test07.txt") 0 #t))
	)
	(test-exit)
)

(define (main argv)
	{let* [(day (if (>= (length argv) 2) (atoi (car argv)) 0))  (filename (if (>= (length argv) 2) (cadr argv) #f))
			(datatext (if filename (read-text-file filename) ""))
		]
		(time (cond
			[(= day 1)  (displayln (aoc01 datatext))
				(displayln (aoc01b datatext))
			]
			[(= day 2)  (displayln (aoc02 datatext))
				(displayln (aoc02b datatext))
			]
			[(= day 3)  (displayln (aoc03 datatext))
				(displayln (aoc03b datatext))
			]
			[(= day 4)  (displayln (aoc04 datatext) #t)
			]
			[(= day 5)  (displayln (aoc05 datatext #t))
				(displayln (hstring (aoc05b datatext #t)))
			]
			[(= day 6)  (displayln (hstring (aoc06 datatext #f)))
				(displayln (aoc06b datatext 10000 #t))
			]
			[(= day 7)  (displayln (aoc07 datatext #t))
				(displayln (aoc07b datatext 60 #t))
			]
			[else (all-tests)]
		))
	}
)
