;; marklib.scm
;; Created 2018-10-20 14:39:00

#<<DOC

## BSD License

Copyright © 2018 by Mark Damon Hughes. All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUDOCENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Introduction

A collection of my utility functions and "how to Scheme" notes.
Anytime I'm likely to re-use something, it gets pulled in here in the appropriate section.

## Style Guide

I'm not interested in/actively hostile to bikeshed arguments about "correct" Scheme style.
This works for me, and makes it easy for me to read.
I've been writing Scheme off and on since the '80s, like this. But I write a lot of other langs, too.

{} around let, do, repeat, until, while, let/cc: Any flow-control block (even if let is just being used for scope).
[] around sub-expressions in cond, let, do.
() around all other expressions.
Prefer `cond` for all multi-line, multi-function choices, use `if` only for ternary choices, `when`/`unless` for single branches.
`begin` is a code smell.
Indent like C with closing paren/brace matched to start if it's more than 1 line.
Indent is tab, displayed as 4 spaces, never mixed with spaces. IT IS AN ERROR to mix tabs and spaces for decoration.
Double space between complex expressions (3 or more parens) in-line, or move to a new line.
lower-case-names for functions, Capitalized for record types, Cap-smallwords for record-related functions, Cap-Words for constants/globals.
Docstring as first element inside each function, #<<DOC … DOC for multi-line comments. Use Markdown, in particular `code` for parameters.

## Known Problems / Workarounds

- Chicken using SRFI numbers for eggs is… inconvenient.
- Chicken's `format` has minimal placeholders, and the LISP-style `format` egg is non-reentrant, so useless in practice. Use built-in format, ~A for all displays, with `pad`, `padnum` defined here to format fields.
- Chicken has no standard docstring system, and recommends putting docs in .egg files. Ha no. Docs further than a few lines from source are always wrong. So I'm adopting the LISP docstring, and will eventually write a tool to read these.

## Feedback

If you have *polite* and *code-related* suggestions, you can reach me at @mdhughes@cybre.space on Fediverse.
If you have *impolite* or *bikeshed* suggestions, I will block you.

## Snippets:

;; unpack list into variables:
(define-values (x y z) (apply values alist)) (format #t "~A,~A,~A~%" x y z)
(set!-values (x y z) (apply values alist)) (format #t "~A,~A,~A~%" x y z)
(let-values [[(x y z) (apply values alist)]] (format #t "xyz:~A,~A,~A~%" q r s x y z))

;; loops:
{let loop [(i 1)]
	(format #t "~A~%" i)
	(when (< i 100)  (loop (add1 i)))
}
{do [(break #f)] [break]
	(set! break #t)
}
{do [(i 0 (add1 i))] [(>= i 10)]
	(format #t "~A~%" i)
}
{do [(break #f) (i 0 (add1 i))] [(or break (>= i 10))]
	(format #t "~A~%" i)
}
{do [(len (anylen coll)) (i 0 (add1 i))] [(>= i len)]
	(format #t "~A: ~A~%" i (anyitem coll i))
}
;; from miscmacros, I don't like these as much but they're short:
;; {dotimes (i 100) (format #t "~A~%" i)} ;; counts 0 to 99
;; {repeat* 100 (format #t "~A~%" it)} ;; counts 100 downto 1

;; exception handling
(handle-exceptions ex (begin
		(display "Error: ") (print-error-message ex)
		#f
	)
	;; normal code here
	(error 'location "text" values…)
)

DOC


(module marklib
	(
		;; Logic
		neq? neqv? nequal? notneg? notnull? !=
		;; Math
		int minmax sign sum
		;; Strings
		atoi pad padnum purify-name
		;; Data Structures
		anycompare anyeq? anyless? anymore? anylen anyitem
		hash-symbol hash-number hash-string hadd! hdel! hget hput! hselect hstring
		list-append! list-delete!
		;; IO
		read-data-file read-text-file
		write-data-file write-text-file
		;; Random
		dice-init die dice dice-stats
		dice-daro dice-taro
		rnd
		;; Text UI
		clear-screen
		displayln
		input
		menu
		msg-init msg msg-title msg-success msg-warning msg-error Window-Size
		print-table
		prompt-string
		yesno
		;; Geometry
		make-Point Point? Point-x Point-x-set! Point-y Point-y-set!
		Point-xy-set! Point-x-add! Point-y-add! Point-copy
		make-Rect Rect? Rect-x Rect-y Rect-w Rect-h Rect-x-set! Rect-y-set! Rect-w-set! Rect-h-set!
		Rect-copy Rect-for-each Rect-inset Rect-intersect Rect-intersect? Rect-origin
		make-Sprite Sprite? Sprite-filename Sprite-sx Sprite-sy Sprite-sw Sprite-sh
		make-Tilemap Tilemap-width Tilemap-height
		Tilemap-init Tilemap-string Tilemap-bounds Tilemap-gridat Tilemap-gridat-set! Tilemap-inbounds?
	)

	(import scheme)
	(import (chicken base))
	(import (chicken condition))
	(import (chicken format))
	(import (chicken io))
	(import (chicken irregex))
	(import (chicken port))
	(import (chicken random))
	(import (chicken read-syntax))
	(import (chicken sort))
	(import (chicken string))
	;; eggs
	(import miscmacros)
	(import srfi-1) ;; lists
	(import srfi-13) ;; strings
	(import srfi-69) ;; NICE. hash-tables
	(import ioctl)
	(import (prefix ansi-escape-sequences "ansi-"))

;;______________________________________
;; Logic

;; Shorthand for (not (comparator …))
(define (neq? a b) (not (eq? a b)))
(define (neqv? a b) (not (eqv? a b)))
(define (nequal? a b) (not (equal? a b)))
(define (notneg? a) (not (negative? a)))
(define (notnull? a) (not (null? a)))
(define (!= a b) (not (= a b)))

;;______________________________________
;; Math

(define (int n)
"Converts a floating point number `n` to an integer."
	(inexact->exact (truncate n))
)

(define (minmax a b n)
"Returns `n` clipped to min/max range `a`…`b`"
	(max a (min n b))
)

(define (sign x)
"Returns 1 if `x` > 0, -1 if `x` < 0, else 0."
	(cond [(positive? x) 1]
		[(negative? x) -1]
		[else 0]
	)
)

(define (sum ls)
"Adds all numbers in `ls`"
	(foldl + 0 ls)
)

;;______________________________________
;; Strings

(define (atoi s . default-value)
#<<DOC
Converts a string to an integer, 0 or `default-value` if invalid.
Strips all leading non-number (0-9, -) elements, and everything from the first trailing non-number to end.
DOC
	{let* [(trimmed (irregex-replace "^([^\\d-]+)(-?[\\d]*)(.*)$" s 2))  (n (string->number trimmed))]
		(if (eqv? n #f)  (if (null? default-value) 0 (car default-value))  (int n) )
	}
)

(define (padnum n padding)
"Renders number `n` as string and pads as `pad` below."
	(pad (number->string n) padding)
)

(define (pad s padding)
"Renders a string with spaces left if `padding` is positive, or right if `padding` is negative, 0 for no padding."
	{let [(slen (string-length s))]
		(cond [(and (positive? padding) (< slen padding))
				(string-append (make-string (- padding slen) #\space) s)
			]
			[(and (negative? padding) (< slen (- padding)))
				(string-append s (make-string (- (- padding) slen) #\space))
			]
			[else s]
		)
	}
)

(define (purify-name s)
"Replaces all non-letter chars with -."
	(irregex-replace/all "^-+" (irregex-replace/all "[^A-Za-z-]+" s "-") "")
)

;;______________________________________
;; Data Structures

(define (anycompare a b)
"Compares any two atoms, returns -1, 0, 1 by ordered comparison, or #f if mismatched or non-atomic types"
	(cond
		[(and (number? a) (number? b))  (if (< a b) -1 (if (> a b) 1 0))]
		[(and (string? a) (string? b))  (if (string<? a b) -1 (if (string>? a b) 1 0))]
		[(and (symbol? a) (symbol? b))  (anycompare (symbol->string a) (symbol->string b))]
		[(and (char? a) (char? b))  (if (char<? a b) -1 (if (char>? a b) 1 0))]
		[(and (boolean? a) (boolean? b))  (if (eq? a b) 0 (if (eq? a #f) -1 1))] ;; #f < #t
		[else #f]
	)
)

(define (anyeq? a b)
	(= (anycompare a b) 0)
)

(define (anyless? a b)
	(< (anycompare a b) 0)
)

(define (anymore? a b)
	(> (anycompare a b) 0)
)

(define (anylen coll)
"Returns length of any collection `coll` (string, list, vector, hash), #f if unknown type"
	(cond
		[(list? coll) (length coll)]
		[(string? coll) (string-length coll)]
		[(vector? coll) (vector-length coll)]
		[(hash-table? coll) (hash-table-size coll)]
		[else #f]
	)
)

(define (anyitem coll i)
"Returns subelement `i` of any collection `coll` (string, list, vector, hash), #f if unknown type"
	(cond
		[(list? coll) (list-ref coll i)]
		[(string? coll) (string-ref coll i)]
		[(vector? coll) (vector-ref coll i)]
		[(hash-table? coll) (hash-table-ref coll i)]
		[else #f]
	)
)

(define (hash-symbol)
"Creates a symbol-keyed hash table"
	(make-hash-table eq? symbol-hash 13)
)

(define (hash-number)
"Creates a number-keyed hash table"
	(make-hash-table = number-hash 13)
)

(define (hash-string)
"Creates a string-keyed hash table"
	(make-hash-table string=? string-hash 13)
)

(define (hadd! ht key n)
#<<DOC
Adds `n` to current value of `key` in hash table `ht`, default 0.
(hadd! ht key 1) accumulates values in a histogram.
DOC
	(hput! ht key (+ (hget ht key 0) n))
)

(define (hdel! ht key)
"Deletes a value for `key` from hash table `ht`"
	(hash-table-delete! ht key)
)

(define (hget ht key . default-value)
"Returns a value for `key` from hash table `ht`, or `default` if not found (default of default is #f)."
	{let [(value (if (null? default-value) #f (car default-value)))]
		(hash-table-ref/default ht key value)
	}
)

(define (hput! ht key value)
"Sets `value` for `key` in hash table `ht`."
	(hash-table-set! ht key value)
)

(define (hselect ht testfunc)
"Returns a key in hash table `ht` where the value fulfils `(testfunc value)`, or #f if not found."
	{let loop [(keys (hash-table-keys ht))]
		(cond
			[(null? keys)  #f]
			[(apply testfunc (list (hget ht (car keys))))  (car keys)]
			[else  (loop (cdr keys))]
		)
	}
)

(define (hstring ht)
"Returns a string representation of hash table `ht`."
	(with-output-to-string (lambda ()
		(for-each (lambda (key)
				(format #t "~A: ~A~%" key (hash-table-ref ht key))
			)
			(sort (hash-table-keys ht) anyless?)
		)
	))
)

;; Puts a new value `value` at the end of a list `ls`
(define-syntax list-append!
	(syntax-rules ()
		[(_ ls value)	(set! ls (append ls (list value)))]
	)
)

;; Deletes value `value` from a list `ls`
(define-syntax list-delete!
	(syntax-rules ()
		[(_ ls value)	(set! ls (delete value ls))]
	)
)

;;______________________________________
;; IO

(define (read-data-file filename)
"Reads a single datum from `filename` and closes it, returns datum or #f if an error occurred."
	(handle-exceptions ex
		(begin
			(msg-error "Error reading data file ~A: " filename)
			(print-error-message ex)
			#f
		)
		{let* [(port (open-input-file filename)) (data (read port))]
			(close-input-port port)
			data
		}
	)
)

(define (read-text-file filename)
"Reads entire text contents from `filename` and closes it, returns text or #f if an error occurred."
	(handle-exceptions ex
		(begin
			(msg-error "Error reading text file ~A: " filename)
			(print-error-message ex)
			#f
		)
		{let* [(port (open-input-file filename)) (text (read-string #f port))]
			(close-input-port port)
			text
		}
	)
)

(define (write-data-file filename data)
"Writes a single datum `data` to `filename` and closes it, returns #t, or #f if an error occurred."
	(handle-exceptions ex
		(begin
			(msg-error "Error writing data file ~A: " filename)
			(print-error-message ex)
			#f
		)
		{let [(port (open-output-file filename))]
			(write data port)
			(close-output-port port)
			#t
		}
	)
)

(define (write-text-file filename text)
"Writes a string `text` to `filename` and closes it, returns #t, or #f if an error occurred."
	(handle-exceptions ex
		(begin
			(msg-error "Error writing text file ~A: " filename)
			(print-error-message ex)
			#f
		)
		{let [(port (open-output-file filename))]
			(write-string text #f port)
			(close-output-port port)
			#t
		}
	)
)

;;______________________________________
;; Random

(define (dice-init)
"Call before using dice."
	;; used to call randomize, still could but random's startup should use system-random.
	void
)

(define (die s)
"Rolls 1 `s`-sided die"
	(add1 (pseudo-random-integer s))
)

(define (dice n s)
"Rolls `n` `s`-sided dice."
	(cond
		[(= n 1) (die s)]
		[else (+ (die s) (dice (sub1 n) s))]
	)
)

(define (dice-stats n s count)
#<<DOC
Generates a vector table of `count` die rolls of `n` d `s` to count of results.
Use (print-table tbl) to display results.
DOC
	{let [(tbl (make-vector (* n (add1 s)) 0))]
		{repeat count
			{let [(r (dice n s))]
				(vector-set! tbl r (add1 (vector-ref tbl r)))
			}
		}
		tbl
	}
)

(define (dice-daro out)
"Rolls 2d6, doubles add and roll over. Pass `out` to print the die roll, #t for stdout, #f to be silent."
	{let [(a (die 6))  (b (die 6))]
		(when out (format out "(~A,~A)" a b))
		(cond
			[(!= a b) (+ a b)]
			[else (+ a b (dice-daro out))]
		)
	}
)

(define (dice-taro out)
"Rolls 3d6, triples add and roll over. Pass `out` to print the die roll, #t for stdout, #f to be silent."
	{let [(a (die 6))  (b (die 6))  (c (die 6))]
		(when out (format out "(~A,~A,~A)" a b c))
		(cond
			[(not (= a b c)) (+ a b c)]
			[else (+ a b c (dice-taro out))]
		)
	}
)

(define (rnd)
"Returns a random real number from 0…<1"
	(pseudo-random-real)
)

;;______________________________________
;; Text UI

(define (clear-screen)
"Clears screen and homes cursor with ANSI"
	(display (ansi-erase-display))
	(display (ansi-cursor-position 1 1))
	(flush-output)
)

(define (displayln obj)
"Convenience to display & newline a single object."
	(display obj) (newline)
)

(define (input prompt)
"Displays prompt if not #f, reads a line from stdin, ends program on EOF"
	(when prompt (format #t "~A~!" prompt))
	{let [(s (read-line))]
		(cond
			[(eof-object? s) (format #t "Bye!~%") (exit)]
			[else s]
		)
	}
)

(define (menu prompt items cancel)
#<<DOC
Displays a 1-indexed menu of `items`, and an input `prompt`.
Returns item chosen (0-indexed), or -1 if `cancel` is true, repeats prompt if invalid.
Example: (menu "Letter? " '("A" "B" "C") #t)
DOC
	(newline)
	(when cancel (format #t "0) Cancel~%"))
	{do [(i 0 (add1 i))] [(>= i (anylen items))]
		(format #t "~A) ~A~%" (add1 i) (anyitem items i))
	}
	{let loop [(sel -1)]
		(set! sel (atoi (input prompt) -1))
		(cond
			[(and (>= sel (if cancel 0 1)) (<= sel (anylen items)))  (sub1 sel)]
			[else (loop sel)]
		)
	}
)

;; Known window size as a Point, will be set in startup if possible
(define Window-Size #f)

(define (msg-init)
"Call before using any `msg` function."
	{let [(winsize (ioctl-winsize))]
		(set! Window-Size (make-Point (cadr winsize) (car winsize))) ;; ioctl is backwards
	}
)

(define (msg text . args)
#<<DOC
Takes a format pattern and args, word-wraps to window, and paginates long text.
Always adds a trailing newline.
DOC
	;; TODO: word-wrap at window, count lines, and paginate
	(apply format #t text args)
	(newline)
)

(define (msg-title text . args)
"Calls `msg` with yellow-on-black text."
	(apply msg (ansi-set-text '(bg-black fg-yellow bold) text) args)
)

(define (msg-success text . args)
"Calls `msg` with green-on-black text."
	(apply msg (ansi-set-text '(bg-black fg-green) text) args)
)

(define (msg-warning text . args)
"Calls `msg` with magenta-on-black text."
	(apply msg (ansi-set-text '(bg-black fg-magenta) text) args)
)

(define (msg-error text . args)
"Calls `msg` with red-on-black text."
	(apply msg (ansi-set-text '(bg-black fg-red) text) args)
)

(define (print-table tbl)
"Prints a tab-delimited table of index, value"
	{do [(len (anylen tbl)) (i 1 (add1 i))] [(>= i len)]
		(format #t "~A\t~A~%" i (anyitem tbl i))
	}
)

(define (prompt-string s)
"Returns `s` colored cyan for a user prompt."
	(ansi-set-text '(bg-black fg-cyan) s)
)

(define (yesno prompt)
"Displays `prompt` and waits for user to type a line starting y or n, case-insensitive. Returns #t or #f"
	{let loop [(ans "")]
		(set! ans (input prompt))
		(cond [(substring-ci=? ans "y" 0 0 1) #t]
			[(substring-ci=? ans "n" 0 0 1) #f]
			[else (msg-warning "Answer Yes or No.") (loop ans)]
		)
	}
)

;;______________________________________
;; Geometry

;; Point type
(define-record Point x y)
(define-reader-ctor 'Point make-Point)
(define-record-printer (Point pt out)
	(format out "#,(Point ~S ~S)" (Point-x pt) (Point-y pt) )
)

(define (Point-x-add! pt n)
	(Point-x-set! pt (+ (Point-x pt) n))
)

(define (Point-y-add! pt n)
	(Point-y-set! pt (+ (Point-y pt) n))
)

(define (Point-xy-set! pt x y)
	(Point-x-set! pt x)
	(Point-y-set! pt y)
)

(define (Point-copy pt)
	(make-Point (Point-x pt) (Point-y pt))
)

;; Rect type
(define-record Rect x y w h)
(define-reader-ctor 'Rect make-Rect)
(define-record-printer (Rect r out)
	(format out "#,(Rect ~S ~S ~S ~S)" (Rect-x r) (Rect-y r) (Rect-w r) (Rect-h r) )
)

(define (Rect-copy rect)
	(make-Rect (Rect-x rect) (Rect-y rect) (Rect-w rect) (Rect-h rect))
)

(define (Rect-for-each rect filter)
"Calls (filter pt) for each point in `rect`"
	{let [(pt (make-Point 0 0))  (x0 (Rect-x rect))  (x1 (+ (Rect-x rect) (Rect-w rect)))  (y0 (Rect-y rect))  (y1 (+ (Rect-y rect) (Rect-h rect)))]
		{do [(y y0 (add1 y))] [(>= y y1)]
			{do [(x x0 (add1 x))] [(>= x x1)]
				(Point-xy-set! pt x y)
				(filter pt)
			}
		}
	}
)

(define (Rect-inset rect dx-left dx-right dy-top dy-bottom)
#<<DOC
Returns `rect` inset by left, right, top, buttom.
+delta to shrink, -delta to grow.
DOC
	(make-Rect (+ (Rect-x rect) dx-left) (+ (Rect-y rect) dy-top) (- (Rect-w rect) (+ dx-left dx-right)) (- (Rect-h rect) dy-top dy-bottom))
)

(define (Rect-intersect arect brect)
"Returns a new rect of the intersection between `arect` and `brect`, coords & size 0 if not intersecting."
	{let [(x (max (Rect-x arect) (Rect-x brect)))  (x2 (min (+ (Rect-x arect) (Rect-w arect)) (+ (Rect-x brect) (Rect-w brect))))
		(y (max (Rect-y arect) (Rect-y brect)))  (y2 (min (+ (Rect-y arect) (Rect-h arect)) (+ (Rect-y brect) (Rect-h brect))))]
		(cond
			[(and (> x2 x) (> y2 y))  (make-Rect x y (- x2 x) (- y2 y))]
			[else (make-Rect 0 0 0 0)]
		)
	}
)

(define (Rect-intersect? arect brect)
"Returns true if `arect` intersects `brect`"
	(not (or (>= (Rect-x arect) (+ (Rect-x brect) (Rect-w brect)))
		(< (+ (Rect-x arect) (Rect-w arect)) (Rect-x brect))
		(>= (Rect-y arect) (+ (Rect-y brect) (Rect-h brect)))
		(< (+ (Rect-y arect) (Rect-h arect)) (Rect-y brect))
	))
)

(define (Rect-origin rect)
"Returns origin point of `rect`"
	(make-Point (Rect-x rect) (Rect-y rect))
)

;; Sprite type: image, source rectangle
(define-record Sprite filename sx sy sw sh)
(define-reader-ctor 'Sprite make-Sprite)
(define-record-printer (Sprite spr out)
	(format out "#,(Sprite ~S ~S ~S ~S ~S)"
		(Sprite-filename spr)
		(Sprite-sx spr)
		(Sprite-sy spr)
		(Sprite-sw spr)
		(Sprite-sh spr)
	)
)

;; Tilemap type: 2D array
(define-record Tilemap width height (setter grids))
(define-reader-ctor 'Tilemap make-Tilemap)
(define-record-printer (Tilemap tm out)
	(format out "#,(Tilemap ~S ~S ~S)" (Tilemap-width tm) (Tilemap-height tm) (Tilemap-grids tm))
)

(define (Tilemap-init width height value)
"Creates an empty Tilemap filled with `value`"
	(make-Tilemap width height (make-vector (* width height) value))
)

(define (Tilemap-string tm . grid-string)
"Creates a string containing all the content of tilemap, optionally passed through grid-string function."
	(with-output-to-string (lambda ()
		{let [(pt (make-Point 0 0))  (gridfunc (if (null? grid-string) identity (car grid-string)))]
			{do [(y 0 (add1 y))] [(>= y (Tilemap-height tm))]
				(Point-y-set! pt y)
				{do [(x 0 (add1 x))] [(>= x (Tilemap-width tm))]
					(Point-x-set! pt x)
					(format #t "~A " (apply gridfunc (list (Tilemap-gridat tm pt))) )
				}
				(newline)
			}
		}
	))
)

(define (Tilemap-bounds tm)
"Returns a Rect of the bounds of the Tilemap"
	(make-Rect 0 0 (Tilemap-width tm) (Tilemap-height tm))
)

(define (Tilemap-gridat tm pt)
"Returns grid at `pt` or #f if out of bounds."
	(cond
		[(Tilemap-inbounds? tm pt)
			{let [(i (+ (Point-x pt) (* (Point-y pt) (Tilemap-width tm))))]
				(vector-ref (Tilemap-grids tm) i)
			}
		]
		[else #f]
	)
)

(define (Tilemap-gridat-set! tm pt g)
"Sets grid at `pt` to `g` if in bounds."
	(cond
		[(Tilemap-inbounds? tm pt)
			{let [(i (+ (Point-x pt) (* (Point-y pt) (Tilemap-width tm))))]
				(vector-set! (Tilemap-grids tm) i g)
			}
		]
		[else #f]
	)
)

(define (Tilemap-inbounds? tm pt)
"Returns #t if pt is inside the map"
	(and (>= (Point-x pt) 0) (< (Point-x pt) (Tilemap-width tm)) (>= (Point-y pt) 0) (< (Point-y pt) (Tilemap-height tm)))
)

) ;; end module marklib
